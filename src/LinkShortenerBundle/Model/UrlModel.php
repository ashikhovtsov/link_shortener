<?php

namespace LinkShortenerBundle\Model;

use Doctrine\ORM\EntityManager;
use LinkShortenerBundle\Entity\Url;
use LinkShortenerBundle\Repository\UrlRepository;

class UrlModel
{
    /**
     * @var string
     */
    protected $alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /**
     * @var int
     */
    protected $base;

    /**
     * @var UrlRepository
     */
    protected $urlRepo;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * UrlModel constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->base = strlen($this->alphabet);

        $this->em      = $em;
        $this->urlRepo = $em->getRepository('LinkShortenerBundle:Url');
    }

    /**
     * Take an id from db if url exists
     * or create new record and encode id to string
     *
     * @param string $url
     * @return string
     */
    public function encodeUrl($url)
    {
        $urlEntity = $this->urlRepo->findUrl($url);

        if (!$urlEntity) {
            $urlEntity = new Url();
            $urlEntity->setUrl($url);

            $this->em->persist($urlEntity);
            $this->em->flush();
        }

        return $this->encodeInt($urlEntity->getId());
    }

    /**
     * Decode short link to url
     *
     * @param string $shortLink
     * @return bool|string
     */
    public function decodeShortLink($shortLink)
    {
        $id = $this->decodeString($shortLink);

        /** @var Url $url */
        $url = $this->urlRepo->find($id);

        if (!$url) {
            return false;
        }

        return $url->getUrl();
    }

    /**
     * Encode int to string value
     *
     * @param int $id
     * @return string
     */
    private function encodeInt($id)
    {
        $result = "";

        while ($id > 0) {
            $result = $this->alphabet[($id - 1) % $this->base] . $result;
            $id = intval($id / $this->base);
        }

        return $result;
    }

    /**
     * Decode string to int value
     *
     * @param string $string
     * @return int
     */
    private function decodeString($string)
    {
        $result = 0;

        $chars = str_split($string);

        foreach ($chars as $char) {
            $result = $result * $this->base + strpos($this->alphabet, $char) + 1;
        }


        return $result;
    }

}
