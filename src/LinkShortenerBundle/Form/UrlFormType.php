<?php

namespace LinkShortenerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UrlFormType extends AbstractType
{
    public function buildForm( FormBuilderInterface $builder, array $options )
    {
        $builder->add('url', TextType::class,
            array(
                'attr' => array(
                    'placeholder' => 'Введите ссылку',
                ),
                'label' => false
            )
        );
    }

    function getName() {
        return 'UrlFormType';
    }
}
