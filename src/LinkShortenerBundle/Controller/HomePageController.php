<?php

namespace LinkShortenerBundle\Controller;

use LinkShortenerBundle\Form\UrlFormType;
use LinkShortenerBundle\Model\UrlModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class HomePageController extends Controller
{
    /**
     * @Route("/{shortLink}", defaults={"shortLink" = ""}, name="homepage")
     */
    public function indexAction($shortLink)
    {
        $passedLink = false;

        if ($shortLink) {
            /** @var UrlModel $urlModel */
            $urlModel = $this->container->get('url_model');

            $url = $urlModel->decodeShortLink($shortLink);

            if ($url) {
                return $this->redirect($url);
            }

            $passedLink = true;
        }

        $urlForm = $this->createForm(urlFormType::class);

        return $this->render(
            'LinkShortenerBundle:HomePage:homepage.html.twig',
            [
                'passedLink' => $passedLink,
                'urlForm'    => $urlForm->createView()
            ]
        );
    }
}
