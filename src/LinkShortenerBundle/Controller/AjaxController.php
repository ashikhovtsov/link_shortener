<?php

namespace LinkShortenerBundle\Controller;

use LinkShortenerBundle\Model\UrlModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    /**
     * @Route("/encodeUrl", name="ajax.encode_url")
     */
    public function encodeUrlAction(Request $request)
    {
        $url = $request->request->get('url');

        if (!$url) {
            return $this->failedResponse('Вы пытаетесь укоротить пустую ссылку');
        }

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            return $this->failedResponse('Вы пытаетесь укоротить не ссылку');
        }

        /** @var UrlModel $urlModel */
        $urlModel = $this->container->get('url_model');

        $shortLink = $urlModel->encodeUrl($url);

        $data['shortLink'] = $request->getSchemeAndHttpHost() . '/' . $shortLink;

        return $this->successResponse($data);
    }

    /**
     * @param $data mixed
     *
     * @return JsonResponse
     */
    protected function successResponse($data = array())
    {
        return new JsonResponse(array('success' => true, 'data' => $data));
    }

    /**
     * @param $message string
     *
     * @return JsonResponse
     */
    protected function failedResponse($message = '')
    {
        $key = 'message';

        if (is_array($message))
        {
            $key = 'messages';
        }

        $response = new JsonResponse(array('success' => false, $key => $message));

        //$response->setStatusCode('400');

        return $response;
    }
}
