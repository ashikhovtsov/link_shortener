function postForm(form, callback){
    $.ajax({
        type        : form.attr('method'),
        url         : form.attr('action'),
        data        : {'url' : form.find('input#url_form_url').val()},
        success     : function(data) {
            callback( data );
        }
    });
}

$(document).ready(function(){
    $('form[name="url_form"]').submit(function(e){
        e.preventDefault();

        postForm($(this), function(response){
            $('div.passedLink').hide();

            if (response.success) {
                $('div.error').hide();

                $('div.form div.shortLink a').text(response.data.shortLink);
                $('div.form div.shortLink').fadeIn();
            } else {
                $('div.form div.shortLink').hide();

                $('div.error').text(response.message)
                $('div.error').fadeIn();
            }
        });

        return false;
    });

    $('div.form a').on('click', function(e) {
        e.preventDefault();

        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(this).text()).select();
        document.execCommand("copy");
        $temp.remove();
    })

});
