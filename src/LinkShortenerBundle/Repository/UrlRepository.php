<?php

namespace LinkShortenerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use LinkShortenerBundle\Entity\Url;

class UrlRepository extends EntityRepository
{
    /**
     * @param $url
     * @return null|Url
     */
    public function findUrl($url)
    {
        return $this->findOneBy([
            'url' => $url
        ]);
    }

}
