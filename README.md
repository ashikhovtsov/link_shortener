Инструкция по установке:

1. Создать БД для проекта

2. Дать права на папки
sudo chmod -R 777 var/cache
sudo chmod -R 777 var/logs
sudo chmod -R 777 var/sessions

3. Composer install
После установки пакетов ввести данные о БД. Остальные поля оставить по дефолту

4. Сгенерировать таблицы в БД
php bin/console doc:sch:update --force

5. Собрать стили и JS
php bin/console assetic:dump

6. Запустить сервер
php bin/console server:start

7. Перейти по адресу который появился в консоле 
([OK] Server listening on http://127.0.0.1:8000)